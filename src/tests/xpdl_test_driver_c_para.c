#include "mpi.h"

#include "xpdl_c_types.h"

#include <stdio.h>
#include <stdlib.h>

void print_v(XPDL_CFLOAT v){
#if XPDL_HAVE_ARITH_C
  printf("%f+%f i", v.r, v.i);
#endif
#if XPDL_HAVE_ARITH_D
  printf("%lf", v);
#endif
#if XPDL_HAVE_ARITH_S
  printf("%f", v);
#endif
#if XPDL_HAVE_ARITH_Z
  printf("%lf+%lf i", v.r, v.i);
#endif
}

XPDL_CFLOAT float_val(double v){
#if XPDL_HAVE_ARITH_C
  XPDL_CFLOAT out = { .r = (float) v, .i = (float) 0 };
  return out;
#endif
#if XPDL_HAVE_ARITH_D
  return v;
#endif
#if XPDL_HAVE_ARITH_S
  return (float) v;
#endif
#if XPDL_HAVE_ARITH_Z
  XPDL_CFLOAT out = { .r = v, .i = (double) 0 };
  return out;
#endif
}

int main(){

  int rank, size;
  struct xpdl_paddle_t_c pdls;

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if(rank == 0) printf("Small test with prints\n");
  if(rank == 0) printf("Initialization phase\n");

  if(size < 2){
    fprintf(stderr, "Error: should be launched with more than one process");
    return 1;
  }

  pdls.job = 0; // Initialize
  pdls.comm = MPI_COMM_WORLD;
  xpdl_driver_c(&pdls);

  pdls.nrhs = 1;
  pdls.verbose = 1;

  if(rank == 0){
    pdls.n = 40;
    pdls.nnz = 3*40-2;

    pdls.i = (int *) malloc(pdls.nnz *sizeof(int));
    pdls.j = (int *) malloc(pdls.nnz *sizeof(int));
    pdls.v = (XPDL_CFLOAT *) malloc(pdls.nnz *sizeof(XPDL_CFLOAT));
    pdls.b_i = (int *) malloc(pdls.n *sizeof(int));
    pdls.b_v = (XPDL_CFLOAT *) malloc(pdls.n *sizeof(XPDL_CFLOAT));

    int cnt_nnz = 0;
    for(int k = 1; k < pdls.n; ++k){
      pdls.i[cnt_nnz] = k;
      pdls.j[cnt_nnz] = k;
      pdls.v[cnt_nnz] = float_val(2);
      cnt_nnz++;
      pdls.i[cnt_nnz] = k+1;
      pdls.j[cnt_nnz] = k;
      pdls.v[cnt_nnz] = float_val(-1);
      cnt_nnz++;
      pdls.i[cnt_nnz] = k;
      pdls.j[cnt_nnz] = k+1;
      pdls.v[cnt_nnz] = float_val(-1);
      cnt_nnz++;
    }
    pdls.i[cnt_nnz] = pdls.n;
    pdls.j[cnt_nnz] = pdls.n;
    pdls.v[cnt_nnz] = float_val(2);

    for(int k = 0; k < pdls.n; ++k){
      pdls.b_i[k] = k+1;
      pdls.b_v[k] = float_val((double) -k);
    }
  }
  else{
    pdls.n = 0;
    pdls.nnz = 0;
    pdls.i =   NULL;
    pdls.j =   NULL;
    pdls.v =   NULL;
    pdls.b_i = NULL;
    pdls.b_v = NULL;
  }

  pdls.icntl[ICNTL_PART_LIB]   = PART_LIB_PTSCOTCH;
  pdls.icntl[ICNTL_CHECKDIST]  = CHECKDIST_YES;
  pdls.icntl[ICNTL_OUTPUTDIST] = OUTPUTDIST_YES;
  pdls.icntl[ICNTL_INITGUESS] = NO_INITGUESS;
  pdls.icntl[ICNTL_ENTRY_MODE] = ENTRY_CENTRALIZED;
  pdls.icntl[ICNTL_RHS] = HAS_RHS;
  pdls.icntl[ICNTL_SOLVER] = SOLVER_MAPHYS;

  if(rank == 0) printf("Pretreatment, domain decomposition and redistribution\n");
  pdls.job = 9;
  xpdl_driver_c(&pdls);

  if(rank == 0){
    free(pdls.i);
    free(pdls.j);
    free(pdls.v);
    free(pdls.b_i);
    free(pdls.b_v);
  }

  int loc_nnz;
  xpdl_pdls_sm_nnz(&loc_nnz);

  int * a_i = (int *) malloc(loc_nnz * sizeof(int));
  int * a_j = (int *) malloc(loc_nnz * sizeof(int));
  XPDL_CFLOAT * a_v = (XPDL_CFLOAT *) malloc(loc_nnz * sizeof(XPDL_CFLOAT));

  xpdl_pdls_sm_ijv(a_i, a_j, a_v);

  int myndof, mynbvi, mysizeintrf, myindexintrf_size;
  xpdl_get_map_domain_sizes(&myndof, &mynbvi, &mysizeintrf, &myindexintrf_size);

  for(int r = 0; r < size; ++r){
    if(rank == r){
      printf("Proc %d (%d x %d)\n", rank, myndof, myndof);
      for(int k = 0; k < loc_nnz; ++k){
        printf("I: %d, J: %d V: ", a_i[k], a_j[k]);
        print_v(a_v[k]);
        printf("\n");
      }
      printf("\n");
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

  int * myinterface  = (int *) malloc(mysizeintrf * sizeof(int));
  int * myindexvi    = (int *) malloc(mynbvi * sizeof(int));
  int * myptrindexvi = (int *) malloc((mynbvi + 1) * sizeof(int));
  int * myindexintrf = (int *) malloc(myindexintrf_size * sizeof(int));

  xpdl_get_map_domain_arrays(myinterface, myindexvi, myptrindexvi, myindexintrf);

  for(int r = 0; r < size; ++r){
    if(rank == r){
      printf("Proc %d\n", rank);
      printf("myinterface: ");
      for(int k = 0; k < mysizeintrf; ++k){
        printf("%d, ", myinterface[k]);
      }
      printf("\nmyindexvi: ");
      for(int k = 0; k < mynbvi; ++k){
        printf("%d, ", myindexvi[k]);
      }
      printf("\nmyptrindexvi: ");
      for(int k = 0; k < mynbvi+1; ++k){
        printf("%d, ", myptrindexvi[k]);
      }
      printf("\nmyindexintrf: ");
      for(int k = 0; k < myindexintrf_size; ++k){
        printf("%d, ", myindexintrf[k]);
      }

      printf("\n");
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

  if(rank == 0) printf("Redist RHS and IG\n");
  pdls.job = 6;
  xpdl_driver_c(&pdls);

  int * rhs_i = (int *) malloc(myndof * sizeof(int));
  int * rhs_j = (int *) malloc(myndof * sizeof(int));
  XPDL_CFLOAT * rhs_buf = (XPDL_CFLOAT *) malloc(myndof * sizeof(XPDL_CFLOAT));
  xpdl_pdls_sm_rhs_ijv(rhs_i, rhs_j, rhs_buf);
  free(rhs_i);
  free(rhs_j);

  for(int r = 0; r < size; ++r){
    if(rank == r){
      printf("Proc %d RHS\n", rank);
      for(int k = 0; k < myndof; ++k){
        print_v(rhs_buf[k]);
        printf("\n");
      }
      printf("---\n");
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

  pdls.sol_i = (int *) malloc(myndof * sizeof(int));
  pdls.sol_v = (XPDL_CFLOAT *) malloc(myndof * sizeof(XPDL_CFLOAT));

  for(int k = 0; k < myndof; ++k){
    pdls.sol_i[k] = k + 1;
    pdls.sol_v[k] = float_val((double) 10-k);
  }

  if(rank == 0) printf("Redist sol into initial distribution\n");
  pdls.job = 7;
  xpdl_driver_c(&pdls);

  int sol_nnz;
  xpdl_pdls_sm_sol_nnz(&sol_nnz);

  int * my_sol_i = (int *) malloc(sol_nnz * sizeof(int));
  int * my_sol_j = (int *) malloc(sol_nnz * sizeof(int));
  XPDL_CFLOAT * my_sol_v = (XPDL_CFLOAT *) malloc(sol_nnz * sizeof(XPDL_CFLOAT));

  xpdl_pdls_sm_sol_ijv(my_sol_i, my_sol_j, my_sol_v);
  free(my_sol_i);
  free(my_sol_j);

  for(int r = 0; r < size; ++r){
    if(rank == r){
      printf("Proc %d SOL\n", rank);
      for(int k = 0; k < sol_nnz; ++k){
        print_v(my_sol_v[k]);
        printf("\n");
      }
      printf("---\n");
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

  if(rank == 0) printf("Finalization phase\n");
  pdls.job = -1; // Finalize
  xpdl_driver_c(&pdls);

  MPI_Finalize();

  free(rhs_buf);
  free(a_i);
  free(a_j);
  free(a_v);
  free(myinterface );
  free(myindexvi   );
  free(myptrindexvi);
  free(myindexintrf);

  free(pdls.sol_i);
  free(pdls.sol_v);

  return 0;
}
