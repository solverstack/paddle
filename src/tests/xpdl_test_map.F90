! \file xpdl_test_map.F90
!> Unitary tests on maps_t structure & tools
!

#include "xpdl_defs.h"
program xpdl_test_map
  Use xpdl_map_mod
  Use xpdl_matgen_mod
  implicit none
  include "mpif.h"

  TYPE(mympi_t) :: mympi
  call mympi_init(mympi,MPI_COMM_WORLD)
  call map_unit_test(mympi)
  call mympi_finalize(mympi)
  write(*,*) "Test passed"

contains

  subroutine map_unit_test(mympi)
    implicit none
    ! Args
    ! Intent(in)
    type(mympi_t), intent(in) :: mympi
    ! Local var
    type(map_t) :: map
    type(map_t) :: extract
    type(sm_t) :: sm
    integer :: n,i
    integer, dimension(1:4) :: indices
    logical :: b

    call map_nullify(map)
    if(mympi%rank==0) then
       n = 10
       call matgen_rand(mympi,n,1./n,sm)
       ! Retrieving user's initial distribution
       call map_from_sm(mympi,sm,map)
       call map_print(map)
       call sm_free(sm)

       ! Test sort
       do i = 1, map%n
          map%key(i)%perm= n-i+1
       end do
       call map_print(map)

       call map_sort_perm(map)
       call map_print(map)

       call map_sort(map)
       call map_print(map)

       ! Test duplicates
       map%n = map%n+2
       call map_resize(map)
       call key_add_domain(2,map%key(map%n-1))
       map%key(map%n-1)%perm = 10
       map%key(map%n-1)%level = 1
       call key_add_domain(3,map%key(map%n))
       map%key(map%n)%perm = 10
       map%key(map%n)%level = 2
       call map_print(map)
       b = map_has_duplicate(map)
       write(*,*) "Last map has duplicate? ", b


       call map_duplicates(map)
       call map_print(map)
       b = map_has_duplicate(map)
       write(*,*) "Last map has duplicate? ", b
       write(*,*) "Mem for last map (bytes): ", map_getmem(map)

       call map_nullify(extract)
       indices = (/1,11,2,7/)
       call map_extract(map,indices,extract)
       call map_print(extract)

       call map_free(map)
       extract%key(2)%level=1
       call map_extract_level(extract,1,map)
       call map_print(map)
       call map_acc(map,extract)
       call map_duplicates(map)
       call map_print(map)

       call map_rcpy(map,extract)

       b = map_is_eq(map,extract)
       write(*,*) "map eq extract? ", b

       call map_nullify(extract)
       indices = (/1,11,3,7/)
       call map_extract(map,indices,extract)
       call map_print(map)
       call map_print(extract)
       b = map_size_is_eq(map,extract)
       write(*,*) "size map eq size extract? ", b
       b = map_is_eq(map,extract)
       write(*,*) "map eq extract? ", b
       call map_free(map)
       call map_print(map)
    end if
    print *, "Starting complicated test"
    n = 15
    call matgen_rand(mympi,n,2./n,sm)
    ! Retrieving user's initial distribution
    call sm_print(sm)
    call map_from_sm(mympi,sm,map)
    call map_print(map)
    call compute_initial_map(mympi,n,sm,map)
    call map_print(map)
    call sm_free(sm)
    call map_free(map)


  end subroutine map_unit_test
end program xpdl_test_map
