###
#
# @copyright (c) 2009-2014 The University of Tennessee and The University
#                          of Tennessee Research Foundation.
#                          All rights reserved.
# @copyright (c) 2012-2016 Inria. All rights reserved.
# @copyright (c) 2012-2014 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
#
###
#
#  @file GenPkgConfig.cmake
#
#  @project MORSE
#  MORSE is a software package provided by:
#     Inria Bordeaux - Sud-Ouest,
#     Univ. of Tennessee,
#     King Abdullah Univesity of Science and Technology
#     Univ. of California Berkeley,
#     Univ. of Colorado Denver. 
#
#  @version 0.9.0
#  @author Cedric Castagnede
#  @author Emmanuel Agullo
#  @author Mathieu Faverge
#  @author Florent Pruvost
#  @author Matthieu Kuhn
#  @date 09-09-2016
#
###

###
#
# GENERATE_PKGCONFIG_FILE: generate a file .pc according to the options
#
###
macro(GENERATE_PKGCONFIG_FILE _file)

    # The link flags specific to this package and any required libraries
    # that don't support PkgConfig
    set(PADDLE_PKGCONFIG_LIBS "")
    # The link flags for private libraries required by this package but not
    # exposed to applications
    set(PADDLE_PKGCONFIG_LIBS_PRIVATE "")
    # A list of packages required by this package
    set(PADDLE_PKGCONFIG_REQUIRED "")
    # A list of private packages required by this package but not exposed to
    # applications
    set(PADDLE_PKGCONFIG_REQUIRED_PRIVATE "")

    list(APPEND PADDLE_PKGCONFIG_LIBS -lpaddle)

    list(APPEND PADDLE_PKGCONFIG_LIBS_PRIVATE ${PADDLE_EXTRA_LIBRARIES})

    list(REMOVE_DUPLICATES PADDLE_PKGCONFIG_LIBS)
    list(REMOVE_DUPLICATES PADDLE_PKGCONFIG_LIBS_PRIVATE)
    list(REMOVE_DUPLICATES PADDLE_PKGCONFIG_REQUIRED)
    list(REMOVE_DUPLICATES PADDLE_PKGCONFIG_REQUIRED_PRIVATE)

    string(REPLACE ";" " " PADDLE_PKGCONFIG_LIBS "${PADDLE_PKGCONFIG_LIBS}")
    string(REPLACE ";" " " PADDLE_PKGCONFIG_LIBS_PRIVATE "${PADDLE_PKGCONFIG_LIBS_PRIVATE}")
    string(REPLACE ";" " " PADDLE_PKGCONFIG_REQUIRED "${PADDLE_PKGCONFIG_REQUIRED}")
    string(REPLACE ";" " " PADDLE_PKGCONFIG_REQUIRED_PRIVATE "${PADDLE_PKGCONFIG_REQUIRED_PRIVATE}")

    # Create .pc file
    # ---------------
    set(_output_file "${CMAKE_BINARY_DIR}/paddle.pc")
    # TODO: add url of MORSE releases in .pc file
    configure_file("${_file}" "${_output_file}" @ONLY)

    # installation
    # ------------
    INSTALL(FILES ${_output_file} DESTINATION lib/pkgconfig)

endmacro(GENERATE_PKGCONFIG_FILE)

##
## @end file GenPkgConfig.cmake
##
