!> \file mympi.F90
!>     Module for MPI handling.
!
!> @brief Defines mympi type to keep  MPI data, namely rank and number
!> of procs. Also defines subroutines for allocation, deallocation and
!> error handling.
!
!     Author: M. Kuhn

module pdl_mympi_mod
  use pdl_error_mod
  implicit none

  type :: mympi_t; sequence
     ! Global data
     integer :: nprocs              !< Global number of processes.
     integer :: comm                !< Global MPI communicator.
     ! Local data
     integer :: rank                !< Current process rank.
     ! Not responsible of init / finalize
     logical :: responsible         !< .true. if responsible of mpi initialization, .false. otherwise.
  end type mympi_t

contains

  !> MPI initialization subroutine.
  !! @brief Initializes MPI if required, gets rank, number of processes and communicator.
  !! @param mympi mympi_t structure to hold current process rank and number of processes. Precondition: none.
  subroutine mympi_init(mympi,comm)
    implicit none
    include "mpif.h"

    ! Arg
    ! Intent(in)
    integer, intent(in) :: comm
    ! Intent(inout)
    type(mympi_t), intent(inout) :: mympi
    ! Local var
    logical :: is_init
    integer :: ierror

    call mpi_initialized(is_init,ierror)
    call mympi_chkerr(ierror)
    if(.not. is_init)then
       mympi%responsible = .true.
       call mpi_init_thread(MPI_THREAD_MULTIPLE,is_init,ierror)
       call mympi_chkerr(ierror)
    else
       mympi%responsible = .false.
    end if

    mympi%comm = comm
    call mpi_comm_size(mympi%comm, mympi%nprocs, ierror)
    call mympi_chkerr(ierror)

    call mpi_comm_rank(mympi%comm, mympi%rank, ierror)
    call mympi_chkerr(ierror)
  end subroutine mympi_init

  !> MPI finalization subroutine.
  !! @brief Finalizes MPI if required.
  !! @param mympi mympi_t structure holding current process rank and number of processes. Precondition: must be allocated.
  subroutine mympi_finalize(mympi)
    implicit none
    include "mpif.h"
    ! Arg
    ! intent(inout)
    type(mympi_t), intent(inout) :: mympi
    ! Local var
    integer :: ierror

    call pdl_assert(mympi_allocated(mympi), "Error, mympi not allocated")
    if(mympi%responsible) then
       call mpi_finalize(ierror)
       call mympi_chkerr(ierror)
    end if
  end subroutine mympi_finalize


  !> MPI print subroutine.
  !! @brief Prints mympi data.
  !! @param mympi mympi_t structure holding current process rank and number of processes. Precondition: must be allocated.
  subroutine mympi_print(mympi)
    implicit none
    include "mpif.h"
    ! Intent(in)
    type(mympi_t), intent(in) :: mympi
    call pdl_assert(mympi_allocated(mympi), "Error, mympi not allocated")
    write(*,*) "mympi structure"
    write(*,*) "  Number of procs: ",mympi%nprocs
    write(*,*) "  Rank ID: ",mympi%rank
    write(*,*) "  Communicator: ",mympi%comm
  end subroutine mympi_print


  !----------------------------------------------!
  !                 Functions                    !
  !----------------------------------------------!

  !> MYMPI allocation request.
  !! @param mympi mympi_t structure to be requested.
  !! @return true if allocated, false otherwise.
  logical function mympi_allocated(mympi)
    implicit none
    type(mympi_t), intent(in) :: mympi
    mympi_allocated = mympi%nprocs>0
  end function mympi_allocated

  !> MYMPI validity request.
  !! @param mympi mympi_t structure to be checked.
  !! @return true if valid, false otherwise.
  logical function mympi_is_valid(mympi)
    implicit none
    type(mympi_t), intent(in) :: mympi
    mympi_is_valid=mympi%rank >=0 .and. mympi%rank < mympi%nprocs
  end function mympi_is_valid


  !> MPI error handling
  !! @param ierr error parameter comming from an mpi_...() return
  subroutine mympi_chkerr(ierr)
    implicit none
    include "mpif.h"
    integer, intent(in) ::  ierr
    if (ierr .ne. mpi_success) then
      write(0,*) " MPI error, code: ",ierr
      call abort
    end if
  end subroutine mympi_chkerr



  !> Node distribution over MPI processes
  !! @brief Computes the node distribution on each MPI process in case
  !! of a distribution by row.
  !! @param nprocs integer number of MPI processes. Precondition: \f$ nprocs > 0 \f$
  !! @param nnodes integer number of nodes. Precondition: \f$ nnodes \geq 0 \f$
  !! @param vtxdist integer array storing number of rows on each process. See description in ParMETIS 4.0.x manual. Precondition: must not be allocated.
  Subroutine get_nodedist(nprocs,nnodes,vtxdist)
    implicit none
    ! Arg
    ! Intent in
    integer, intent(in)                           :: nprocs
    integer, intent(in)                           :: nnodes
    ! Intent out
    integer, intent(out), dimension(:), allocatable :: vtxdist
    ! Local var
    integer :: i

    call pdl_assert(nprocs > 0, "Error, nprocs < 1")
    call pdl_assert(nnodes >= 0, "Error, nnodes < 0")
    call pdl_assert(.not. allocated(vtxdist), "Error, vtxdist already allocated")

    allocate(vtxdist(1:nprocs+1))

    vtxdist(1)=1
    do i = 1,nprocs
       vtxdist(i+1)=vtxdist(i)+get_number_of_local_nodes(i-1,nprocs,nnodes)
    end do
  end subroutine get_nodedist

  !> Number of local nodes
  !! @brief Gives  the number of  local nodes for the  distribution by
  !! line as a function of the  MPI rank, the number of processes, and
  !! the number of nodes.
  !! @param rank integer current MPI process rank. Precondition: \f$ rank \geq 0 \f$
  !! @param nprocs integer number of MPI processes. Precondition: \f$ nprocs > 0 \f$
  !! @param nnodes integer number of nodes. Precondition: \f$ nnodes \geq 0 \f$
  integer function get_number_of_local_nodes(rank,nprocs,nnodes)
    implicit none
    ! Arg
    ! Intent in
    integer, intent(in) :: rank, nprocs
    integer, intent(in) :: nnodes

    call pdl_assert(rank >= 0, "Error, rank < 0")
    call pdl_assert(nprocs > 0, "Error, nprocs < 1")
    call pdl_assert(nnodes >= 0, "Error, nnodes < 0")
    get_number_of_local_nodes = (nnodes+nprocs-1-rank)/nprocs
  end function get_number_of_local_nodes

end module pdl_mympi_mod
