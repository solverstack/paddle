#include "mpi.h"

#ifndef XPDL_C_TYPES_HEADER_DEF
#define XPDL_C_TYPES_HEADER_DEF

#ifdef __cplusplus
extern "C" {
#endif

#ifndef PDL_C_TYPES_HEADER_DEF
#define PDL_C_TYPES_HEADER_DEF

#define PDL_ICNTL_SIZE   11
#define PDL_TINFO_SIZE   8
#define PDL_SMINFO_SIZE  4
#define PDL_RHSINFO_SIZE 3
#define PDL_MINFO_SIZE   7

  // Redefine ICNTL with C numbering (starting at 0)

#define ICNTL_OUTPUT_ERR  0 //< Fortran unit to print error messages.
#define ICNTL_OUTPUT_STD  1 //< Fortran unit to print std messages.
#define ICNTL_VERBOSITY   2 //< Level of verbosity for paddle.
#define ICNTL_PART_LIB    3 //< Library used for parallel partitionement
#define ICNTL_PART_STRAT  4 //< Parallel partitionement strategy (ND, K-way, ...)
#define ICNTL_ENTRY_MODE  5 //< Entry mode: centralized or distributed
#define ICNTL_RHS         6
#define ICNTL_INITGUESS   7
#define ICNTL_SOLVER      8
#define ICNTL_CHECKDIST   9
#define ICNTL_OUTPUTDIST  10 //< Enable/disable the output of the domain decomposition, csv graph format

  // PART parameters
#define PART_LIB_SCOTCH   1
#define PART_LIB_PARMETIS 2
#define PART_LIB_PTSCOTCH 3

#define PART_STRAT_ND     1
  //#define PART_STRAT_KWAY 2

  // ENTRY parameters
#define ENTRY_DISTRIBUTED  1
#define ENTRY_CENTRALIZED  2
#define ENTRY_USER_DD      3

#define HAS_RHS 1
#define NO_RHS  2

#define NO_INITGUESS   1
#define HAS_INITGUESS  2

  // SOLVER parameters
#define SOLVER_MAPHYS 1

  // DEBUG parameters
#define CHECKDIST_NO  1
#define CHECKDIST_YES 2

  // OUTPUT parameter
#define OUTPUTDIST_NO  1
#define OUTPUTDIST_YES 2

  // TINFO List (C NUMBERING)
#define TINFO_PRETREAT   0
#define TINFO_PART       1
#define TINFO_MAP        2
#define TINFO_SMDIST     3
#define TINFO_SOLVER     4
#define TINFO_RHSDIST    5
#define TINFO_SOLDIST    6
#define TINFO_TOTAL      7

  // MINFO List (C NUMBERING)
#define MINFO_PRETREAT   = 0
#define MINFO_PART       = 1
#define MINFO_MAP        = 2
#define MINFO_SMDIST     = 3
#define MINFO_SOLVER     = 4
#define MINFO_RHSDIST    = 5
#define MINFO_SOLDIST    = 6

  typedef struct {float r,i;} paddle_complex;
  typedef struct {double r,i;} paddle_complex_double;

#endif // PDL_C_TYPES_HEADER_DEF

  struct xpdl_paddle_t_c{
    int job;

    int icntl[PDL_ICNTL_SIZE];

    double tinfo[5*PDL_TINFO_SIZE];
    int musedinfo[5*PDL_TINFO_SIZE];
    int mpeakinfo[5*PDL_TINFO_SIZE];
    int sminfo[5*PDL_SMINFO_SIZE];
    int rhsinfo[5*PDL_RHSINFO_SIZE];
    int ninfo[5*2];
    int partmempeak;

    int * i;
    int * j;
    XPDL_CFLOAT * v;

    int n;
    int nnz;
    int n_glob;
    int nrhs;

    int * b_i;
    XPDL_CFLOAT * b_v;

    int * ig_i;
    XPDL_CFLOAT * ig_v;

    int * sol_i;
    XPDL_CFLOAT * sol_v;

    int verbose;
    MPI_Comm comm;
    MPI_Fint fcomm;
  };

  void xpdl_driver_c(struct xpdl_paddle_t_c * pdls);
  void xpdl_pdls_sm_nnz(int * nnz);
  void xpdl_pdls_sm_rhs_nnz(int * nnz);
  void xpdl_pdls_sm_sol_nnz(int * nnz);

  void xpdl_pdls_sm_ijv(int *, int *, XPDL_CFLOAT *);
  void xpdl_pdls_sm_rhs_ijv(int *, int *, XPDL_CFLOAT *);
  void xpdl_pdls_sm_sol_ijv(int *, int *, XPDL_CFLOAT *);

  void xpdl_get_map_domain_sizes(int *, int *, int *,int *);
  void xpdl_get_map_domain_arrays(int *, int *, int *, int *);

#ifdef __cplusplus
}
#endif
#endif // XPDL_C_TYPES_HEADER_DEF
