/* fortran definitions for PADDLE */

/*! 
  \def PDL_INT
  PADDLE Integer
 */

/*! 
  \def PDL_VAL
  PADDLE Value
 */

#ifndef PDL_DEFS_F_H__
#define PDL_DEFS_F_H__

#ifndef PADDLE_VERSION
#define PADDLE_VERSION '0.3.7'
#endif

#ifdef PDL_DEBUG
#define PDL_ASSRT(a,b) call pdl_assert(a,b)
#else
#define PDL_ASSRT(a,b)
#endif


! Integers
#define PDL_INTKIND         4
#define PDL_INT        INTEGER(KIND=PDL_INTKIND)
#if PDL_INTKIND == 4
#define PDL_MPIINT     MPI_INTEGER
#else
#define PDL_MPIINT     MPI_INTEGER
#endif

! Arithmetic
#define PDL_VALKIND      XPDL_KIND
#define PDL_INTVAL       XPDL_INTVAL
#define PDL_STRVAL       "XPDL_ARITH"
#define PDL_VAL          XPDL_ARITH(kind=PDL_VALKIND)
#define PDL_VALUE(XPDL_ARGS)   XPDL_FUNC(XPDL_ARGS,kind=PDL_VALKIND)
#define PDL_RAND()       PDL_VALUE(XPDL_RAND)
#define PDL_TOL          XPDL_TOL
! global size
#define PDL_STRL        1024

#if PDL_INTVAL == 1
#if XPDL_KIND == 4
#define PDL_MPIVAL MPI_REAL
#else
#define PDL_MPIVAL MPI_DOUBLE_PRECISION
#endif
#else
#if XPDL_KIND == 4
#define PDL_MPIVAL MPI_COMPLEX
#else
#define PDL_MPIVAL MPI_DOUBLE_COMPLEX
#endif
#endif 

#endif 
