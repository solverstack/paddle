module xpdl_paddle_driver_c_mod
  use ISO_C_BINDING
  use pdl_enum_mod
  use xpdl_paddle_mod

  implicit none

  type(xpdl_paddle_t), save :: pdls

contains

  subroutine xpdl_driver_c2f(JOB, ICNTL, TINFO, &
    MUSEDINFO, MPEAKINFO, SMINFO, RHSINFO, NINFO, PARTMEMPEAK, &
    I, J, V, N, NNZ, N_GLOB, NRHS, B_I, B_V, IG_I, IG_V, &
    SOL_I, SOL_V, COMM, VERBOSE) bind(C,name="xpdl_driver_c2f")

    integer(c_int), value :: JOB, COMM, VERBOSE
    integer(c_int) :: ICNTL(PDL_ICNTL_SIZE)

    type(c_ptr), intent(in), value :: I
    type(c_ptr), intent(in), value :: J
    type(c_ptr), intent(in), value :: V
    type(c_ptr), intent(in), value :: B_I
    type(c_ptr), intent(in), value :: B_V
    type(c_ptr), intent(in), value :: IG_I
    type(c_ptr), intent(in), value :: IG_V
    type(c_ptr), intent(in), value :: SOL_I
    type(c_ptr), intent(in), value :: SOL_V

    real(c_double) :: TINFO(5,PDL_TINFO_SIZE)
    integer(c_int) :: MUSEDINFO(5,PDL_TINFO_SIZE)
    integer(c_int) :: MPEAKINFO(5,PDL_TINFO_SIZE)
    integer(c_int) :: SMINFO(5,PDL_SMINFO_SIZE)
    integer(c_int) :: RHSINFO(5,PDL_RHSINFO_SIZE)
    integer(c_int) :: NINFO(5,2)

    type(c_ptr), value :: PARTMEMPEAK
    integer(c_int), value :: N
    integer(c_int), value :: NNZ
    integer(c_int), value :: N_GLOB
    integer(c_int), value :: NRHS

    integer, pointer :: partmempeak_ptr

    pdls%job = JOB
    pdls%comm = COMM
    pdls%icntl(:) = ICNTL(:)
    pdls%nrhs = NRHS
    pdls%verbose = (VERBOSE .ne. 0)

    if(c_associated(V)) then
       pdls%n   = N
       pdls%nnz  = NNZ
       pdls%n_glob = N_GLOB
       call c_f_pointer(I    , pdls%i    , shape=[pdls%nnz])
       call c_f_pointer(J    , pdls%j    , shape=[pdls%nnz])
       call c_f_pointer(V    , pdls%v    , shape=[pdls%nnz])
       call c_f_pointer(B_I  , pdls%b_i  , shape=[pdls%n])
       call c_f_pointer(B_V  , pdls%b_v  , shape=[pdls%n])
       call c_f_pointer(IG_I , pdls%ig_i , shape=[pdls%n])
       call c_f_pointer(IG_V , pdls%ig_v , shape=[pdls%n])
    else
       pdls%n   = 0
       pdls%nnz  = 0
       pdls%n_glob = 0
    endif

    if(c_associated(SOL_V)) then
       call c_f_pointer(SOL_I, pdls%sol_i, shape=[pdls%maphys_domain%myndof])
       call c_f_pointer(SOL_V, pdls%sol_v, shape=[pdls%maphys_domain%myndof])
    end if

    call xpdl_driver(pdls)

    TINFO(:,:)     = pdls%TINFO(:,:)
    MUSEDINFO(:,:) = pdls%MUSEDINFO(:,:)
    MPEAKINFO(:,:) = pdls%MPEAKINFO(:,:)
    SMINFO(:,:)    = pdls%SMINFO(:,:)
    RHSINFO(:,:)   = pdls%RHSINFO(:,:)
    NINFO(:,:)     = pdls%NINFO(:,:)

    call c_f_pointer(PARTMEMPEAK, partmempeak_ptr)
    partmempeak_ptr = pdls%partmempeak

  end subroutine xpdl_driver_c2f

  subroutine xpdl_pdls_sm_nnz_c2f(NNZ) bind(C,name="xpdl_pdls_sm_nnz_c2f")
    type(c_ptr), value :: NNZ
    integer, pointer :: nnz_val
    call c_f_pointer(NNZ, nnz_val)
    nnz_val = pdls%sm%nnz
  end subroutine xpdl_pdls_sm_nnz_c2f

  subroutine xpdl_pdls_sm_ijv_c2f(SM_I, SM_J, SM_V) bind(C,name="xpdl_pdls_sm_ijv_c2f")
    type(c_ptr), value :: SM_I
    type(c_ptr), value :: SM_J
    type(c_ptr), value :: SM_V

    integer, pointer :: i(:)
    integer, pointer :: j(:)
    XPDL_ARITH(kind=XPDL_KIND), pointer :: v(:)

    if(pdls%sm%nnz == 0) return
    call c_f_pointer(SM_I, i, shape=[pdls%sm%nnz])
    call c_f_pointer(SM_J, j, shape=[pdls%sm%nnz])
    call c_f_pointer(SM_V, v, shape=[pdls%sm%nnz])

    i(:) = pdls%sm%i(:)
    j(:) = pdls%sm%j(:)
    v(:) = pdls%sm%v(:)
  end subroutine xpdl_pdls_sm_ijv_c2f

  subroutine xpdl_pdls_sm_rhs_nnz_c2f(NNZ) bind(C,name="xpdl_pdls_sm_rhs_nnz_c2f")
    type(c_ptr), value :: NNZ
    integer, pointer :: nnz_val
    call c_f_pointer(NNZ, nnz_val)
    nnz_val = pdls%rhs%sm%nnz
  end subroutine xpdl_pdls_sm_rhs_nnz_c2f

  subroutine xpdl_pdls_sm_rhs_ijv_c2f(SM_I, SM_J, SM_V) bind(C,name="xpdl_pdls_sm_rhs_ijv_c2f")
    type(c_ptr), value :: SM_I
    type(c_ptr), value :: SM_J
    type(c_ptr), value :: SM_V

    integer, pointer :: i(:)
    integer, pointer :: j(:)
    XPDL_ARITH(kind=XPDL_KIND), pointer :: v(:)

    if(pdls%rhs%sm%nnz == 0) return
    call c_f_pointer(SM_I, i, shape=[pdls%rhs%sm%nnz])
    call c_f_pointer(SM_J, j, shape=[pdls%rhs%sm%nnz])
    call c_f_pointer(SM_V, v, shape=[pdls%rhs%sm%nnz])

    i(:) = pdls%rhs%sm%i(:)
    j(:) = pdls%rhs%sm%j(:)
    v(:) = pdls%rhs%sm%v(:)
  end subroutine xpdl_pdls_sm_rhs_ijv_c2f

  subroutine xpdl_pdls_sm_sol_nnz_c2f(NNZ) bind(C,name="xpdl_pdls_sm_sol_nnz_c2f")
    type(c_ptr), value :: NNZ
    integer, pointer :: nnz_val
    call c_f_pointer(NNZ, nnz_val)
    nnz_val = pdls%sol%sm%nnz
  end subroutine xpdl_pdls_sm_sol_nnz_c2f

  subroutine xpdl_pdls_sm_sol_ijv_c2f(SM_I, SM_J, SM_V) bind(C,name="xpdl_pdls_sm_sol_ijv_c2f")
    type(c_ptr), value :: SM_I
    type(c_ptr), value :: SM_J
    type(c_ptr), value :: SM_V

    integer, pointer :: i(:)
    integer, pointer :: j(:)
    XPDL_ARITH(kind=XPDL_KIND), pointer :: v(:)

    integer :: k

    call c_f_pointer(SM_I, i, shape=[pdls%sol%sm%nnz])
    call c_f_pointer(SM_J, j, shape=[pdls%sol%sm%nnz])
    call c_f_pointer(SM_V, v, shape=[pdls%sol%sm%nnz])

    do k = 1, pdls%sol%sm%nnz
       i(k) = pdls%sol%sm%i(k)
       j(k) = pdls%sol%sm%j(k)
       v(k) = pdls%sol%sm%v(k)
    end do
  end subroutine xpdl_pdls_sm_sol_ijv_c2f

  subroutine xpdl_get_map_domain_sizes_c2f(myndof, mynbvi, mysizeintrf, myindexintrf_size) bind(C, name="xpdl_get_map_domain_sizes_c2f")
    type(c_ptr), value :: myndof, mynbvi, mysizeintrf, myindexintrf_size
    integer, pointer   :: fmyndof, fmynbvi, fmysizeintrf, fmyindexintrf_size

    call c_f_pointer(myndof, fmyndof)
    call c_f_pointer(mynbvi, fmynbvi)
    call c_f_pointer(mysizeintrf, fmysizeintrf)
    call c_f_pointer(myindexintrf_size, fmyindexintrf_size)

    fmyndof      = pdls%maphys_domain%myndof
    fmynbvi      = pdls%maphys_domain%mynbvi
    fmysizeintrf = pdls%maphys_domain%mysizeintrf
    fmyindexintrf_size = pdls%maphys_domain%myptrindexVi(pdls%maphys_domain%mynbvi+1)-1

  end subroutine xpdl_get_map_domain_sizes_c2f

  subroutine xpdl_get_map_domain_arrays_c2f(myinterface, myindexvi, myptrindexvi, myindexintrf) &
       bind(C, name="xpdl_get_map_domain_arrays_c2f")

    type(c_ptr), value :: myinterface, myindexvi, myptrindexvi, myindexintrf
    integer, pointer   :: fmyinterface(:), fmyindexvi(:), fmyptrindexvi(:), fmyindexintrf(:)
    integer :: myindexintrf_size

    myindexintrf_size = pdls%maphys_domain%myptrindexVi(pdls%maphys_domain%mynbvi+1)-1

    call c_f_pointer(myinterface , fmyinterface , shape=[pdls%maphys_domain%mysizeintrf])
    call c_f_pointer(myindexvi   , fmyindexvi   , shape=[pdls%maphys_domain%mynbvi])
    call c_f_pointer(myptrindexvi, fmyptrindexvi, shape=[pdls%maphys_domain%mynbvi+1])
    call c_f_pointer(myindexintrf, fmyindexintrf, shape=[myindexintrf_size])

    fmyinterface (:) = pdls%maphys_domain%myinterface (:)
    fmyindexvi   (:) = pdls%maphys_domain%myindexvi   (:)
    fmyptrindexvi(:) = pdls%maphys_domain%myptrindexvi(:)
    fmyindexintrf(:) = pdls%maphys_domain%myindexintrf(:)

  end subroutine xpdl_get_map_domain_arrays_c2f

end module xpdl_paddle_driver_c_mod
