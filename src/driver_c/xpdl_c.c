#include "xpdl_c_types.h"
#include "mpi.h"

extern void xpdl_driver_c2f(int JOB, int * ICNTL, double *TINFO,
                            int * MUSEDINFO, int * MPEAKINFO, int * SMINFO, int * RHSINFO, int * NINFO, int * PARTMEMPEAK,
                            int * I, int * J, XPDL_CFLOAT * V, int N, int NNZ, int N_GLOB, int NRHS,
                            int * B_I, XPDL_CFLOAT *B_V, int * IG_I, XPDL_CFLOAT * IG_V,
                            int * SOL_I, XPDL_CFLOAT * SOL_V, MPI_Fint COMM, int VERBOSE);

extern void xpdl_pdls_sm_nnz_c2f(int * nnz);
extern void xpdl_pdls_sm_rhs_nnz_c2f(int * nnz);
extern void xpdl_pdls_sm_sol_nnz_c2f(int * nnz);

extern void xpdl_pdls_sm_ijv_c2f(int *, int *, XPDL_CFLOAT *);
extern void xpdl_pdls_sm_rhs_ijv_c2f(int *, int *, XPDL_CFLOAT *);
extern void xpdl_pdls_sm_sol_ijv_c2f(int *, int *, XPDL_CFLOAT *);

extern void xpdl_get_map_domain_sizes_c2f(int * myndof, int * mynbvi, int * mysizeintrf,int * myindexintrf_size);

extern void xpdl_get_map_domain_arrays_c2f(int * myinterface, int * myindexvi,
                                           int * myptrindexvi, int * myindexintrf);

void xpdl_driver_c(struct xpdl_paddle_t_c * pdls){

  (pdls->fcomm) = MPI_Comm_c2f((pdls->comm));

  if(pdls->job == 0){
    for(int k = 0; k < PDL_ICNTL_SIZE; ++k) pdls->icntl[k] = 0;
    pdls->i = NULL;
    pdls->j = NULL;
    pdls->v = NULL;

    pdls->n = 0;
    pdls->nnz = 0;
    pdls->n_glob = 0;
    pdls->nrhs = 0;

    pdls->b_i   = NULL;
    pdls->b_v   = NULL;
    pdls->ig_i  = NULL;
    pdls->ig_v  = NULL;
    pdls->sol_i = NULL;
    pdls->sol_v = NULL;

    pdls->verbose = 0;
  }

  xpdl_driver_c2f((pdls->job), pdls->icntl, pdls->tinfo,
                  pdls->musedinfo, pdls->mpeakinfo, pdls->sminfo, pdls->rhsinfo, pdls->ninfo, &(pdls->partmempeak),
                  pdls->i, pdls->j, pdls->v, pdls->n, pdls->nnz, pdls->n_glob, pdls->nrhs, pdls->b_i, pdls->b_v, pdls->ig_i, pdls->ig_v,
                  pdls->sol_i, pdls->sol_v, pdls->fcomm, pdls->verbose);

}

void xpdl_pdls_sm_nnz    (int * nnz){ xpdl_pdls_sm_nnz_c2f(nnz); }
void xpdl_pdls_sm_rhs_nnz(int * nnz){ xpdl_pdls_sm_rhs_nnz_c2f(nnz); }
void xpdl_pdls_sm_sol_nnz(int * nnz){ xpdl_pdls_sm_sol_nnz_c2f(nnz); }

void xpdl_pdls_sm_ijv    (int *i, int *j, XPDL_CFLOAT *v){ xpdl_pdls_sm_ijv_c2f(i, j, v); }
void xpdl_pdls_sm_rhs_ijv(int *i, int *j, XPDL_CFLOAT *v){ xpdl_pdls_sm_rhs_ijv_c2f(i, j, v); }
void xpdl_pdls_sm_sol_ijv(int *i, int *j, XPDL_CFLOAT *v){ xpdl_pdls_sm_sol_ijv_c2f(i, j, v); }

void xpdl_get_map_domain_sizes(int * myndof, int * mynbvi, int * mysizeintrf,int * myindexintrf_size){
  xpdl_get_map_domain_sizes_c2f(myndof, mynbvi, mysizeintrf, myindexintrf_size);
}

void xpdl_get_map_domain_arrays(int * myinterface, int * myindexvi,
                                int * myptrindexvi, int * myindexintrf){
  xpdl_get_map_domain_arrays_c2f(myinterface, myindexvi, myptrindexvi, myindexintrf);
}
