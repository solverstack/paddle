!
!*Module for MaPHyS interfacing.
!
!> In this module, we define the required domain
! structure for maphys.
!
!> @author: M. Kuhn
module pdl_maphys_type
  use pdl_mympi_mod
  implicit none
  
  !> To declare local domain structure for maphys
  type :: maphys_domain_t; sequence      
    integer :: domid               !< Domain ID
    integer :: myndof              !< Local number of degree of freedom
    integer :: mysizeintrf         !< Local number of nonzero entries
    integer :: mynbvi              !< Local number of neighbor subdomains                    

    integer, dimension(:), allocatable :: myinterface      !< Local interface
    integer, dimension(:), allocatable :: myindexvi    !< Local line indices in global numbering
    integer, dimension(:), allocatable :: myptrindexvi !< Local column indices in global numbering
    integer, dimension(:), allocatable :: myindexintrf !< Local values : v_loc(i) = A(i_loc(i),j_loc(i))
 end type maphys_domain_t
contains

  !> Domain structure allocation
  !! @param domid integer ID of current domain. Precondition: \f$ domid > 0 \f$
  !! @param myndof integer number of degree of freedom. Precondition: \f$ myndof >= 0 \f$
  !! @param mysizeintrf integer number of degree of freedom on the interface of the domain.
  !! Precondition: \f$ mysizeintrf >= 0 \f$
  !! @param mynbvi integer number of neighbors of the domain. Precondition: \f$ mynbvi >= 0 \f$
  !! @param lenindintrf integer size of myindexintrf. Precondition: \f$ lenindintrf >= 0 \f$
  !! @param domain domain_t domain to be allocated. Precondition: must not be allocated
  subroutine maphys_domain_allocate(domid,myndof,mysizeintrf,mynbvi,lenindintrf,maphys_domain)
    implicit none
    ! Arguments
    ! Intent in
    integer, intent(in) :: domid
    integer, intent(in) :: myndof
    integer, intent(in) :: mysizeintrf
    integer, intent(in) :: mynbvi
    integer, intent(in) :: lenindintrf !< size of myindexintrf
    ! Intent out
    TYPE(maphys_domain_t), intent(out) :: maphys_domain

    call pdl_assert(domid>0, "Error, domid <= 0")
    call pdl_assert(myndof>=0, "Error, myndof < 0")
    call pdl_assert(mysizeintrf>=0, "Error, mysizeintrf < 0")
    call pdl_assert(mynbvi>=0, "Error, mynbvi < 0")
    call pdl_assert(lenindintrf>=0, "Error, lenindintrf < 0")
    call pdl_assert(maphys_domain_not_allocated(maphys_domain), "Error, maphys_domain already allocated")
    
    maphys_domain%domid = domid
    maphys_domain%myndof = myndof
    maphys_domain%mysizeintrf = mysizeintrf
    maphys_domain%mynbvi = mynbvi

    allocate(maphys_domain%myinterface(1:mysizeintrf))
    allocate(maphys_domain%myindexvi(1:mynbvi))
    allocate(maphys_domain%myptrindexvi(1:mynbvi+1))
    allocate(maphys_domain%myindexintrf(1:lenindintrf))
    ! Set to zero
    maphys_domain%myinterface(1:mysizeintrf)  = 0
    maphys_domain%myindexvi(1:mynbvi)         = 0
    maphys_domain%myptrindexvi(1:mynbvi+1)    = 0
    maphys_domain%myindexintrf(1:lenindintrf) = 0
  end subroutine maphys_domain_allocate

  !> Domain free subroutine
  !! @param domain domain_t structure to be copied. Precondition: none.
  subroutine maphys_domain_free(maphys_domain)
    implicit none
    ! Argument
    ! Intent inout
    TYPE(maphys_domain_t), intent(out) :: maphys_domain

    maphys_domain%domid = 0
    maphys_domain%myndof = 0
    maphys_domain%mysizeintrf = 0
    maphys_domain%mynbvi = 0
    if(allocated(maphys_domain%myinterface))then
       deallocate(maphys_domain%myinterface)
    end if
    if(allocated(maphys_domain%myindexvi))then
       deallocate(maphys_domain%myindexvi)
    end if
    if(allocated(maphys_domain%myptrindexvi))then
       deallocate(maphys_domain%myptrindexvi)
    end if
    if(allocated(maphys_domain%myindexintrf))then
       deallocate(maphys_domain%myindexintrf)
    end if
  end subroutine maphys_domain_free

    !> Domain free subroutine
  !! @param domain domain_t structure to be copied. Precondition: none.
  subroutine maphys_domain_nullify(maphys_domain)
    implicit none
    ! Argument
    ! Intent inout
    TYPE(maphys_domain_t), intent(out) :: maphys_domain

    call maphys_domain_free(maphys_domain)
  end subroutine maphys_domain_nullify

  
  !> Domain copy subroutine
  !! @param domain domain_t structure to be copied. Precondition: must be allocated.
  !! @param res domain_t copy of domain. Precondition: must not be allocated.  
  subroutine maphys_domain_cpy(maphys_domain,res)
    implicit none
    ! Intent in
    TYPE(maphys_domain_t), intent(in) :: maphys_domain
    ! Intent out
    TYPE(maphys_domain_t), intent(out) :: res
    
    call pdl_assert(maphys_domain_allocated(maphys_domain), "Error, maphys_domain not allocated")
    call pdl_assert(maphys_domain_not_allocated(res), "Error, res already allocated")

    call maphys_domain_allocate(maphys_domain%domid,maphys_domain%myndof,maphys_domain%mysizeintrf,maphys_domain%mynbvi,&
         maphys_domain%myptrindexvi(maphys_domain%mynbvi+1)-1,res)

    res%myinterface(:) = maphys_domain%myinterface(:)
    res%myindexvi(:) = maphys_domain%myindexvi(:)
    res%myptrindexvi(:) = maphys_domain%myptrindexvi(:)
    res%myindexintrf(:) = maphys_domain%myindexintrf(:)
  end subroutine maphys_domain_cpy


  !> Print domain subroutine
  !! @brief Prints domain data.
  !! @param domain domain_t structure to be printed. Precondition: none.
  subroutine maphys_domain_print(maphys_domain)
    implicit none
    ! Arg
    ! Intent(in)
    TYPE(maphys_domain_t), intent(in) :: maphys_domain
    
    write(*,*) "Maphys_Domain ID: ", maphys_domain%domid
    write(*,*) "  myndof: ", maphys_domain%myndof
    write(*,*) "  mysizeintrf: ", maphys_domain%mysizeintrf
    write(*,*) "  myinterface: ", maphys_domain%myinterface(:)
    write(*,*) "  mynbvi: ", maphys_domain%mynbvi
    write(*,*) "  myindexvi: ", maphys_domain%myindexvi(:)
    write(*,*) "  myptrindexvi: ", maphys_domain%myptrindexvi(:)
    write(*,*) "  myindexintrf: ", maphys_domain%myindexintrf(:)
  end subroutine maphys_domain_print

  subroutine maphys_domain_graph(mympi,maphys_domain)
    implicit none
    include "mpif.h"
    ! Arg
    ! Intent(in)
    TYPE(mympi_t), intent(in) :: mympi
    TYPE(maphys_domain_t), intent(in) :: maphys_domain

    ! Local
    integer, parameter :: oueR=43
    integer, parameter :: ouvR=44
    integer :: i, j
    integer :: nprocs, nadjvi, mpierr
    integer, dimension(:), allocatable :: myndof
    integer, dimension(:), allocatable :: mysizeinterface
    integer, dimension(:), allocatable :: mynbvi
    integer, dimension(:), allocatable :: myscannbvi
    integer, dimension(:), allocatable :: myadjvi
    integer, dimension(:), allocatable :: sizeintrfvi
    integer, dimension(:), allocatable :: mysizeintrfvi

    nprocs = mympi%nprocs
    if(mympi%rank==0) then
       allocate(myndof(1:nprocs))
       allocate(mysizeinterface(1:nprocs))
       allocate(mynbvi(1:nprocs))
       allocate(myscannbvi(1:nprocs+1))
    end if
    call mpi_gather(&
         maphys_domain%myndof,1,MPI_INTEGER,&
         myndof,1,MPI_INTEGER,&
         0,mympi%comm,mpierr&
    )
!    if(mympi%rank==0) write(*,*) "DBG domain graph ndof", myndof
    call mpi_gather(&
         maphys_domain%mysizeintrf,1,MPI_INTEGER,&
         mysizeinterface,1,MPI_INTEGER,&
         0,mympi%comm,mpierr&
    )
!    if(mympi%rank==0) write(*,*) "DBG domain graph sizeintrf", mysizeinterface
    call mpi_gather(&
         maphys_domain%mynbvi,1,MPI_INTEGER,&
         mynbvi,1,MPI_INTEGER,&
         0,mympi%comm,mpierr&
    )
!    if(mympi%rank==0) write(*,*) "DBG domain graph nbvi", mynbvi
    allocate(sizeintrfvi(1:maphys_domain%mynbvi))
    do i = 1, maphys_domain%mynbvi
       sizeintrfvi(i)=maphys_domain%myptrindexvi(i+1)-maphys_domain%myptrindexvi(i)
    end do


    if(mympi%rank==0) then
       myscannbvi(1)=0
       do i = 2, nprocs+1
          myscannbvi(i) = myscannbvi(i-1)+mynbvi(i-1)
       end do
!       write(*,*) "DBG domain graph scan", myscannbvi
       nadjvi=sum(mynbvi(:))
       allocate(myadjvi(1:nadjvi))
       allocate(mysizeintrfvi(1:nadjvi))
    end if
    call mpi_gatherv(&
         maphys_domain%myindexvi,maphys_domain%mynbvi,MPI_INTEGER,&
         myadjvi,mynbvi,myscannbvi,MPI_INTEGER,&
         0, mympi%comm,mpierr&
         )
    call mpi_gatherv(&
         sizeintrfvi,maphys_domain%mynbvi,MPI_INTEGER,&
         mysizeintrfvi,mynbvi,myscannbvi,MPI_INTEGER,&
         0, mympi%comm,mpierr&
         )
!    if(mympi%rank==0) write(*,*) "DBG domain graph ", myadjvi
!    if(mympi%rank==0) write(*,*) "DBG domain graph ", mysizeintrfvi
    if(mympi%rank==0) then
       myscannbvi=myscannbvi+1
       myadjvi=myadjvi+1
       open(unit=oueR,file="edges.csv",action="write",status="replace")
       open(unit=ouvR,file="vertices.csv",action="write",status="replace")
       write(oueR,*) "from,to,weight"
       write(ouvR,*) "vtx,label,weight"
       do i = 1, nprocs
          do j = myscannbvi(i), myscannbvi(i+1)-1
             if(i<myadjvi(j)) then
                write(oueR,'(I0,A,I0,A,I0)') i,",",myadjvi(j),",",mysizeintrfvi(j)
             end if
          end do
          write(ouvR,'(I0,A,I0,A,I0)') i,",",i,",",(myndof(i)+mysizeinterface(i))
       end do
       close(oueR)
       close(ouvR)
    end if


    if(allocated(myndof)) deallocate(myndof)
    if(allocated(mysizeinterface)) deallocate(mysizeinterface)
    if(allocated(mynbvi)) deallocate(mynbvi)
    if(allocated(myscannbvi)) deallocate(myscannbvi)
    if(allocated(myadjvi)) deallocate(myadjvi)
  end subroutine maphys_domain_graph

  !-------------------------------------------------!
  !                Error handling                   !
  !-------------------------------------------------!

  integer function maphys_getmem(maphys)
    implicit none
    ! Arg
    ! Intent(in)
    TYPE(maphys_domain_t), intent(in) :: maphys
    ! Local vars
    integer :: res

    res = 0
    if(maphys%mysizeintrf>0) res = res + int(sizeof(maphys%myinterface),kind=4)
    if(maphys%mynbvi>0) res = res + int(sizeof(maphys%myindexvi) + sizeof(maphys%myptrindexvi),kind=4)
    if(maphys%mynbvi>0 .and. maphys%mysizeintrf>0) res = res + int(sizeof(maphys%myindexintrf),kind=4)
    maphys_getmem = res
  end function maphys_getmem

  
  !> Domain allocation request.
  !! @brief Requests if domain is allocated.
  !! @param domain domain_t matrix to be requested.
  !! @return true if allocated, false otherwise.
  logical function maphys_domain_allocated(maphys_domain)
    implicit none
    ! Arg
    ! Intent(in)
    TYPE(maphys_domain_t), intent(in) :: maphys_domain
    ! Local var
    logical :: res
    res             =       allocated(maphys_domain%myinterface)     &
                      .and. allocated(maphys_domain%myindexvi)       &
                      .and. allocated(maphys_domain%myptrindexvi)    &
                      .and. allocated(maphys_domain%myindexintrf)
    maphys_domain_allocated = res
  end function maphys_domain_allocated

  !> Domain unallocation request.
  !! @brief Requests if domain is not allocated.
  !! @param domain domain_t matrix to be requested.
  !! @return true if not allocated, false otherwise.
  logical function maphys_domain_not_allocated(maphys_domain)
    implicit none
    ! Arg
    ! Intent(in)
    TYPE(maphys_domain_t), intent(in) :: maphys_domain
    ! Local var
    logical :: res
    res             =       (.not. allocated(maphys_domain%myinterface)   )  &
                      .and. (.not. allocated(maphys_domain%myindexvi)     )  &
                      .and. (.not. allocated(maphys_domain%myptrindexvi)  )  &
                      .and. (.not. allocated(maphys_domain%myindexintrf)  ) 
    maphys_domain_not_allocated = res
  end function maphys_domain_not_allocated

end module pdl_maphys_type
